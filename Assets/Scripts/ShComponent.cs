using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class ColorScaleComponent : FindComponent
{
    [SerializeField] private float _scaleMultiplier;
    [SerializeField] private float _scaleDuration;
    [SerializeField] private float _duration;
    private SpriteRenderer _sprite;
    public override void DoEffect(Action callback)
    {
        _sprite = GetComponent<SpriteRenderer>();
        transform.DOScale(transform.localScale * _scaleMultiplier, _scaleDuration);
        _sprite.DOColor(Color.blue, _duration).OnComplete(() =>
        {
            gameObject.SetActive(false);
            callback?.Invoke();
        }); 
    }
}
