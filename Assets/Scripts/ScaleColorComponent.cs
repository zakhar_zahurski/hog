using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;

public class ScaleColorComponent : FindComponent
{
    [SerializeField] private float _scaleMultiplier;
    [SerializeField] private float _duration;
    private SpriteRenderer _sprite;
    public override void DoEffect(Action callback)
    {
        _sprite = GetComponent<SpriteRenderer>();
        transform.DOScale(transform.localScale * _scaleMultiplier, _duration);
        _sprite.DOColor(Color.blue, _duration).OnComplete(() =>
        {
            gameObject.SetActive(false);
            callback?.Invoke();
        });
    }
}
