using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;

public class BlinkingScaleComponent : FindComponent
{
    [SerializeField] private float _scaleMultiplier;
    [SerializeField] private float _scaleDuration;
    [SerializeField] private float _str;
    [SerializeField] private int _vibrato;
    [SerializeField] private float _randomness;
    private SpriteRenderer _sprite;
    public override void DoEffect(Action callback)
    {
        _sprite = GetComponent<SpriteRenderer>();        
        
        transform.DOShakePosition(_scaleDuration, _str, _vibrato, _randomness, false, true).OnComplete(() =>
        {
            gameObject.SetActive(false);
            callback?.Invoke();
        });
    }
}
